FROM pytorch/pytorch:latest
RUN apt-get update -y
# install gdal and pin tiledb (for now):
# https://stackoverflow.com/questions/71904252/gdalinfo-error-while-loading-shared-libraries-libtiledb-so-2-2-cannot-open-sh
RUN conda install gdal libgdal tiledb=2.2
RUN apt-get install vim -y
