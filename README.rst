######################
pytorch-gdal-container
######################

.. begin-inclusion-intro-marker-do-not-remove

Docker container with pre-installed Pytorch and Gdal environment

.. end-inclusion-intro-marker-do-not-remove


.. begin-inclusion-usage-marker-do-not-remove

How to use
----------

Provide user documentation here.

.. end-inclusion-usage-marker-do-not-remove


.. begin-inclusion-installation-marker-do-not-remove

Installation
------------

To install pytorch-gdal-container, do:

.. code-block:: console

  git clone https://gitlab.com/rwsdatalab/public/projects/tools/pytorch-gdal-container.git
  cd pytorch-gdal-container
  pip install .

Run tests (including coverage) with:

.. code-block:: console

  pip install -r requirements-dev.txt
  python setup.py test

.. end-inclusion-installation-marker-do-not-remove


Documentation
-------------

.. _README:

Include a link to your project's full documentation here.


.. begin-inclusion-license-marker-do-not-remove

License
-------

Copyright (c) 2022, Rijkswaterstaat


Licensed under the MIT License.

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.


.. end-inclusion-license-marker-do-not-remove
