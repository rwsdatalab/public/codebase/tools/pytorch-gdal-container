.. pytorch-gdal-container documentation master file

pytorch-gdal-container's documentation
======================================

.. include:: ../README.rst
  :start-after: .. begin-inclusion-intro-marker-do-not-remove
  :end-before: .. end-inclusion-intro-marker-do-not-remove

Getting help
------------

Having trouble? We'd like to help!

- Looking for specific information? Try the :ref:`genindex` or :ref:`modindex`.
- Report bugs with pytorch-gdal-container in our `issue tracker <https://gitlab.com/rwsdatalab/public/projects/tools/pytorch-gdal-container/-/issues>`_.
- See this document as `pdf <pytorch-gdal-container.pdf>`_.

.. toctree::
   :maxdepth: 1
   :caption: First steps

   Installation <installation.rst>
   Usage <usage.rst>

.. toctree::
   :maxdepth: 1
   :caption: All the rest

   API <apidocs/pytorch_gdal_container.rst>
   Contributing <contributing.rst>
   License <license.rst>
   Release notes <changelog.rst>
